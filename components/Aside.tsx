import { HStack, Text, VStack } from "@chakra-ui/layout";
import { Image } from "@chakra-ui/react";
import type { NextPage } from "next";
import { Icon } from "@chakra-ui/icon";
import { IoIosSchool, IoMdContacts } from "react-icons/io";
import { GrAnnounce } from "react-icons/gr";
import { BiMessageCheck } from "react-icons/bi";

const Aside: NextPage = () => {
  return (
    <VStack
      zIndex={999}
      borderTop={["3px solid white", "none", "none"]}
      bg={["primary", "primary", "transparent"]}
      flexDirection={["row", "row", "column"]}
      position={["fixed", "fixed", "revert"]}
      bottom={0}
      left={0}
      right={0}
      as="aside"
      p={["5px", "5px", "40px"]}
      alignItems={["center", "center", "stretch"]}
    >
      <Image
        d={["none", "none", "block"]}
        alt="Yaşar üniversitesi"
        src="/logos/logo-tr.png"
        filter="brightness(0) invert(1)"
      />
      <HStack
        justifyContent="center"
        spacing={5}
        paddingBlock={["10px", "10px", "25px"]}
        width="100%"
      >
        <Icon as={IoIosSchool} color="white" width="30px" height="30px" />
        <Text
          d={["none", "none", "block"]}
          color="white"
          fontSize="lg"
          cursor="pointer"
          whiteSpace="nowrap"
        >
          Ana Sayfa
        </Text>
      </HStack>
      <HStack
        justifyContent="center"
        spacing={5}
        paddingBlock={["10px", "10px", "25px"]}
        width="100%"
      >
        <Icon as={BiMessageCheck} color="white" width="30px" height="30px" />
        <Text
          d={["none", "none", "block"]}
          color="white"
          fontSize="lg"
          cursor="pointer"
          whiteSpace="nowrap"
        >
          Duyurular
        </Text>
      </HStack>
      <HStack
        justifyContent="center"
        spacing={5}
        paddingBlock={["10px", "10px", "25px"]}
        width="100%"
      >
        <Icon as={IoMdContacts} color="white" width="30px" height="30px" />
        <Text
          d={["none", "none", "block"]}
          color="white"
          fontSize="lg"
          cursor="pointer"
          whiteSpace="nowrap"
        >
          İletişim
        </Text>
      </HStack>
    </VStack>
  );
};

export default Aside;
