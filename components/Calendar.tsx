import { Box } from "@chakra-ui/layout";
import type { NextPage } from "next";
import DayPicker from "react-day-picker";
import "react-day-picker/lib/style.css";
import MomentLocaleUtils from "react-day-picker/moment";
import "moment/locale/tr";

const Calendar: NextPage = () => {
  return (
    <Box width="100%">
      <DayPicker localeUtils={MomentLocaleUtils} locale={"tr"} />
    </Box>
  );
};

export default Calendar;
