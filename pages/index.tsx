import type { NextPage } from "next";
import {
  Box,
  Flex,
  Grid,
  VStack,
  Text,
  Heading,
  HStack,
} from "@chakra-ui/layout";
import { Image } from "@chakra-ui/image";
import { Button, Icon, Tooltip } from "@chakra-ui/react";
import { FaUserGraduate } from "react-icons/fa";
import { AiOutlineBook } from "react-icons/ai";
import ExamCard from "@/components/ExamCard";
import Aside from "@/components/Aside";
import Calendar from "@/components/Calendar";

const Home: NextPage = () => {
  return (
    <Box minH="100vh" paddingBlockEnd={["100px", "0", "0"]} bg="primary">
      <Grid
        padding="20px"
        gridGap={5}
        minH="100vh"
        gridTemplateColumns={["1fr", "1fr", "200px 1fr"]}
      >
        <Aside />
        <Flex
          flexDirection={["column", "column", "row"]}
          p={["10px", "10px", "40px"]}
          bg="white"
          borderRadius="30px"
          as="main"
        >
          <VStack alignItems="stretch" flex="3" spacing={7}>
            <VStack
              alignItems="flex-start"
              spacing={5}
              textAlign={["center", "center", "start"]}
            >
              <Image
                d={["block", "block", "none"]}
                alt="Yaşar üniversitesi"
                width="60px"
                alignSelf="center"
                src="/logos/logo-tr.png"
              />

              <Heading as="h1" fontSize="5xl">
                Merhaba,{" "}
                <Text fontWeight="normal" as="span">
                  Akifcan
                </Text>
              </Heading>
              <Text color="gray">
                Buradan sınav duyurularını, sınav tarihlerini takip edebilirsin.
                Gerektiği takdirde bizlere ulaşabilirsin.
              </Text>
            </VStack>
            <VStack
              alignItems="stretch"
              textAlign={["center", "center", "start"]}
            >
              <VStack alignItems="strech">
                <Heading as="h4" fontSize="3xl">
                  Sınavlar
                </Heading>
                <ExamCard />
                <ExamCard />
                <ExamCard />
                <ExamCard />
                <ExamCard />
                <ExamCard />
              </VStack>
            </VStack>
          </VStack>
          <VStack flex="4" alignItems="flex-start">
            <Grid
              d={["block", "block", "grid"]}
              gridGap={5}
              width="100%"
              gridTemplateColumns={["200px 1fr 1fr"]}
              gridTemplateRows={["auto auto"]}
            >
              <Grid
                paddingBlock={[5, 5, 0]}
                gridRow="1/-1"
                gridTemplateRows="100px 25px 25px 200px"
                shadow="md"
                overflow="hidden"
                borderRadius="12px"
              >
                <Box height="150px">
                  <Image
                    objectFit="cover"
                    width="100%"
                    height="100%"
                    alt=""
                    src="https://thumbs.dreamstime.com/b/vector-education-pattern-educattion-seamless-background-vector-education-pattern-educattion-seamless-background-vector-113988685.jpg"
                  />
                </Box>
                <Box gridRow="3/-1" paddingInline={3}>
                  <VStack alignItems="flex-start">
                    <Box
                      borderRadius="md"
                      width="50px"
                      height="50px"
                      bg="red"
                    ></Box>
                    <Text fontWeight="bold">Akifcan Kara</Text>
                    <HStack>
                      <Icon as={FaUserGraduate} color="gray" />
                      <Text color="gray" fontSize="12px">
                        Bilgisayar Programcılığı
                      </Text>
                    </HStack>
                    <VStack
                      width="100%"
                      shadow="md"
                      alignItems="flex-start"
                      borderRadius="md"
                      bg="#eee"
                      flex={1}
                      padding={2}
                    >
                      <Icon as={AiOutlineBook} color="primary" />
                      <Box>
                        <Heading>5</Heading>
                        <Text fontSize="13px">Derslerim</Text>
                      </Box>
                    </VStack>
                  </VStack>
                </Box>
              </Grid>
              <VStack
                marginBlock={[3, 3, 0]}
                alignItems="flex-start"
                bg="#FBE9E7"
                borderRadius="2xl"
                shadow="md"
                gridColumn="2/-1"
                padding={5}
              >
                <Heading>Canlı Destek 🆘</Heading>
                <Text>Yardıma mı ihtiyacınız var?</Text>
                <Button colorScheme="blue" bg="primary" color="white">
                  Bize ulaşın...
                </Button>
              </VStack>
              <Flex
                marginBlock={[3, 3, 0]}
                flexDirection="column"
                alignItems="flex-start"
                justifyContent="center"
                bg="#F9A825"
                shadow="md"
                borderRadius="2xl"
                gridColumn="2"
                padding={5}
              >
                <HStack
                  marginBlockEnd={5}
                  justifyContent="space-between"
                  width="100%"
                >
                  <Box
                    display="grid"
                    placeItems="center"
                    width="50px"
                    height="50px"
                    bg="#FDD835"
                    borderRadius="md"
                  >
                    👩‍🏫
                  </Box>
                </HStack>
                <Text color="white" fontWeight="bold">
                  Danışman Hocanız:
                </Text>
                <Text color="white" fontWeight="normal">
                  Kevin Powell
                </Text>
              </Flex>
              <Flex
                marginBlock={[3, 3, 0]}
                flexDirection="column"
                alignItems="flex-start"
                justifyContent="center"
                bg="#283593"
                shadow="md"
                borderRadius="2xl"
                gridColumn="3"
                padding={5}
              >
                <HStack
                  marginBlockEnd={5}
                  justifyContent="space-between"
                  width="100%"
                >
                  <Box
                    display="grid"
                    placeItems="center"
                    width="50px"
                    height="50px"
                    bg="#3949AB"
                    borderRadius="md"
                  >
                    📢
                  </Box>
                </HStack>
                <Text color="white" fontWeight="bold">
                  Duyurular
                </Text>
                <Text color="white" fontWeight="normal">
                  Duyuruları Görüntüle
                </Text>
              </Flex>{" "}
            </Grid>
            <Calendar />
          </VStack>
        </Flex>
      </Grid>
    </Box>
  );
};

export default Home;
