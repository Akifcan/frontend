import { extendTheme } from "@chakra-ui/react";

// 2. Extend the theme to include custom colors, fonts, etc
const colors = {
  background: "#dedede",
  primary: "#00529C",
};

const fonts = {
  heading: "Quicksand",
  body: "Quicksand",
};

const components = {
  Heading: {
    variants: {},
  },
  Text: {
    variants: {},
  },
};

const theme = extendTheme({ colors, fonts, components });
export default theme;
