import type { NextPage } from "next";
import { VStack, Text, HStack } from "@chakra-ui/layout";
import { Image } from "@chakra-ui/image";
import { Button, Icon, Tooltip } from "@chakra-ui/react";
import { BsPencilFill } from "react-icons/bs";

const ExamCard: NextPage = () => {
  return (
    <HStack paddingInlineEnd={5} spacing={5}>
      <Image src="/icons/database.png" width="50px" alt="veritabanı sınavı" />
      <VStack spacing={0} alignItems="flex-start">
        <Text fontSize="2xl" fontWeight="bold">
          Veritabanı
        </Text>
        <HStack alignItems="flex-start">
          <Text fontSize="md" color="gray" fontWeight="bold">
            Toplam Soru:5
          </Text>
          <Text fontSize="md" color="gray" fontWeight="bold">
            Süre:50dk
          </Text>
        </HStack>
      </VStack>
      <Tooltip label="Başla">
        <Button ml="auto !important" variant="ghost">
          <Icon as={BsPencilFill} color="primary" />
        </Button>
      </Tooltip>
    </HStack>
  );
};

export default ExamCard;
